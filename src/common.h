#ifndef foosydneycommonh
#define foosydneycommonh

/***
  This file is part of libsydney.

  Copyright 2007-2008 Lennart Poettering

  libsydney is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  libsydney is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with libsydney. If not, see
  <http://www.gnu.org/licenses/>.
***/

#include <stdarg.h>

#include "sydney.h"
#include "mutex.h"
#include "macro.h"

typedef struct sa_pcm_attrs {
    sa_pcm_format_t format;
    unsigned rate;
    unsigned nchannels;
    sa_channel_t *channel_map;
} pcm_attrs_t;

struct sa_stream {
    sa_mutex *mutex;

    sa_mode_t mode; /* immutable -- this means accessing this doesn't require locking */

    pcm_attrs_t pcm_attrs; /* immutable, except for .rate */
    size_t pcm_sample_size; /* immutable */
    size_t pcm_frame_size; /* immutable */

    char *codec; /* immutable */

    sa_state_t state;

    sa_proplist *props;

    size_t read_overall_latency;
    size_t read_process_time;
    size_t write_overall_latency;
    size_t write_immutable_latency;
    size_t write_process_time;

    sa_xrun_mode_t xrun_mode;
    sa_bool_t ni_enabled;
    sa_bool_t dynamic_rate_enabled;

    sa_event_callback_t event_callback;

    char *driver;
    char *device;

    int32_t *read_volume;
    int32_t *write_volume;

    sa_event_callback_t callback;
    void *user_data;

    sa_adjust_t adjust_rate;
    sa_adjust_t adjust_nchannels;
    sa_adjust_t adjust_pcm_format;

    /* Current event data */
    sa_event_t event;
    int error;
    sa_notify_t notify;
    void *notify_data;
    size_t notify_data_nbytes;

    void *private; /* driver specific data */
#ifdef HAVE_DSO
    void *private_dso;
#endif
};

size_t sa_get_pcm_sample_size(sa_pcm_format_t f);

float volume_to_dB(int32_t v);
float volume_to_linear(int32_t v);
int32_t volume_from_dB(float dB);
int32_t volume_from_linear(float f);

sa_bool_t sa_stream_frame_aligned(sa_stream *s, int64_t nbytes);

#endif
