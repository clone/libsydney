/***
  This file is part of libsydney.

  Copyright 2007-2008 Lennart Poettering

  libsydney is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  libsydney is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with libsydney. If not, see
  <http://www.gnu.org/licenses/>.
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <inttypes.h>

#include "macro.h"
#include "bswap.h"

static void byteswap16(void *_dst, size_t dstr, const void *_src, size_t sstr, size_t bytes) {
    uint16_t *dst = _dst;
    const uint16_t *src = _src;
    size_t n = bytes / sizeof(uint16_t);

    for (; n > 0; n--) {
        *dst = SA_UINT16_SWAP(*src);

        src += sstr / sizeof(uint16_t);
        dst += dstr / sizeof(uint16_t);
    }
}

static void byteswap24(void *_dst, size_t dstr, const void *_src, size_t sstr, size_t bytes) {
    uint8_t *dst = _dst;
    const uint8_t *src = _src;
    size_t n = bytes / 3;

    for (; n > 0; n--) {
        dst[0] = src[2];
        dst[2] = src[0];

        src += sstr / (sizeof(uint8_t)*3);
        dst += dstr / (sizeof(uint8_t)*3);
    }
}

static void byteswap32(void *_dst, size_t dstr, const void *_src, size_t sstr, size_t bytes) {
    uint32_t *dst = _dst;
    const uint32_t *src = _src;
    size_t n = bytes / sizeof(uint32_t);

    for (; n > 0; n--) {
        *dst = SA_UINT32_SWAP(*src);

        src += sstr / sizeof(uint32_t);
        dst += dstr / sizeof(uint32_t);
    }
}

sa_byteswap_func_t sa_get_byteswap_func(sa_pcm_format_t f) {

    static const sa_byteswap_func_t funcs[_SA_PCM_FORMAT_MAX] = {
        [SA_PCM_FORMAT_S16_RE] = byteswap16,
        [SA_PCM_FORMAT_S24_RE] = byteswap24,
        [SA_PCM_FORMAT_S32_RE] = byteswap32,
        [SA_PCM_FORMAT_FLOAT32_BE] = byteswap32,
    };

    sa_assert(f < _SA_PCM_FORMAT_MAX);

    return funcs[f];
}
