/***
  This file is part of libsydney.

  Copyright 2007-2008 Lennart Poettering

  libsydney is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  libsydney is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with libsydney. If not, see
  <http://www.gnu.org/licenses/>.
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "driver.h"
#include "common.h"
#include "malloc.h"

int driver_open(sa_stream *s) {
    return SA_ERROR_NOTAVAILABLE;
}

int driver_destroy(sa_stream *s) {
    return SA_ERROR_INVALID;
}

int driver_change_write_latency(sa_stream *s, size_t *overall_latency, size_t *immutable_latency, size_t *process_time) {
    return SA_ERROR_INVALID;
}

int driver_change_read_latency(sa_stream *s, size_t *overall_latency, size_t *process_time) {
    return SA_ERROR_INVALID;
}

int driver_change_device(sa_stream *s, const char *device_name) {
    return SA_ERROR_INVALID;
}

int driver_change_read_volume(sa_stream *s, const int32_t vol[]) {
    return SA_ERROR_INVALID;
}

int driver_change_write_volume(sa_stream *s, const int32_t vol[]) {
    return SA_ERROR_INVALID;
}

int driver_change_pcm_rate(sa_stream *s, unsigned rate) {
    return SA_ERROR_INVALID;
}

int driver_change_props(sa_stream *s, sa_proplist *changed, sa_proplist *merged) {
    return SA_ERROR_INVALID;
}

int driver_get_state(sa_stream *s, sa_state_t *state) {
    return SA_ERROR_INVALID;
}

int driver_get_position(sa_stream *s, sa_position_t position, int64_t *pos) {
    return SA_ERROR_INVALID;
}

int driver_wait(sa_stream *s) {
    return SA_ERROR_INVALID;
}

int driver_read(sa_stream *s, void *data, size_t nbytes) {
    return SA_ERROR_INVALID;
}

int driver_read_ni(sa_stream *s, unsigned channel, void *data, size_t nbytes) {
    return SA_ERROR_INVALID;
}

int driver_pwrite(sa_stream *s, const void *data, size_t nbytes, int64_t offset, sa_seek_t whence) {
    return SA_ERROR_INVALID;
}

int driver_pwrite_ni(sa_stream *s, unsigned channel, const void *data, size_t nbytes, int64_t offset, sa_seek_t whence) {
    return SA_ERROR_INVALID;
}

int driver_get_read_size(sa_stream *s, size_t *size) {
    return SA_ERROR_INVALID;
}

int driver_get_write_size(sa_stream *s, size_t *size) {
    return SA_ERROR_INVALID;
}

int driver_resume(sa_stream *s) {
    return SA_ERROR_INVALID;
}

int driver_pause(sa_stream *s) {
    return SA_ERROR_INVALID;
}

int driver_drain(sa_stream *s) {
    return SA_ERROR_INVALID;
}
