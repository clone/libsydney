#ifndef foosydneymallochfoo
#define foosydneymallochfoo

/***
  This file is part of libsydney.

  Copyright 2008 Lennart Poettering

  libsydney is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  libsydney is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with libsydney. If not, see
  <http://www.gnu.org/licenses/>.
***/

#include <stdlib.h>
#include <string.h>

#include "sydney.h"
#include "macro.h"

void* sa_malloc(size_t size);
void* sa_malloc0(size_t size);
void sa_free(void *ptr);
char *sa_strdup(const char *s);
char *sa_strndup(const char *s, size_t n);
void* sa_memdup(const void* p, size_t size);

#define sa_new(t, n) ((t*) sa_malloc(sizeof(t)*(n)))
#define sa_new0(t, n) ((t*) sa_malloc0(sizeof(t)*(n)))
#define sa_newdup(t, p, n) ((t*) sa_memdup(p, sizeof(t)*(n)))

char *sa_sprintf_malloc(const char *format, ...) __attribute__((format(printf, 1, 2)));

#endif
