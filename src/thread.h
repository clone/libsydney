#ifndef foosydneythreadhfoo
#define foosydneythreadhfoo

/***
  This file is part of libsydney.

  Copyright 2007-2008 Lennart Poettering

  libsydney is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  libsydney is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with libsydney. If not, see
  <http://www.gnu.org/licenses/>.
***/

#include "macro.h"

typedef struct sa_thread sa_thread;

typedef void (*sa_thread_func_t) (void *userdata);

sa_thread* sa_thread_new(sa_thread_func_t thread_func, void *userdata);
void sa_thread_free(sa_thread *t);
int sa_thread_join(sa_thread *t);
sa_bool_t sa_thread_is_running(sa_thread *t);
sa_thread *sa_thread_self(void);
void sa_thread_yield(void);

void* sa_thread_get_data(sa_thread *t);
void sa_thread_set_data(sa_thread *t, void *userdata);

typedef struct sa_tls sa_tls;

sa_tls* sa_tls_new(sa_free_cb_t free_cb);
void sa_tls_free(sa_tls *t);
void * sa_tls_get(sa_tls *t);
void *sa_tls_set(sa_tls *t, void *userdata);

#endif
