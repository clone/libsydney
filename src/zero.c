/***
  This file is part of libsydney.

  Copyright 2007-2008 Lennart Poettering

  libsydney is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  libsydney is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with libsydney. If not, see
  <http://www.gnu.org/licenses/>.
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include "macro.h"
#include "zero.h"

static void zero_u8(void *dst, size_t dstr, size_t bytes) {
    uint8_t *d = dst;

    if (dstr == 1)
        memset(dst, 0x80, bytes);
    else {
        for (; bytes > 0; bytes --, d += dstr)
            *d = 0x80;
    }
}

static void zero_ulaw(void *dst, size_t dstr, size_t bytes) {
    uint8_t *d = dst;

    if (dstr == 1)
        memset(dst, 0xff, bytes);
    else {
        for (; bytes > 0; bytes --, d += dstr)
            *d = 0xff;
    }
}

static void zero_alaw(void *dst, size_t dstr, size_t bytes) {
    uint8_t *d = dst;

    if (dstr == 1)
        memset(dst, 0xd5, bytes);
    else {
        for (; bytes > 0; bytes --, d += dstr)
            *d = 0xd5;
    }
}

static void zero_16(void *dst, size_t dstr, size_t bytes) {
    uint16_t *d = dst;
    size_t n = bytes/sizeof(uint16_t);

    if (dstr == sizeof(uint16_t))
        memset(dst, 0, bytes);
    else {
        for (; n > 0; n --, d += dstr/sizeof(uint16_t))
            *d = 0;
    }
}

static void zero_24(void *dst, size_t dstr, size_t bytes) {
    uint8_t *d = dst;
    size_t n = bytes/3;

    if (dstr == 3)
        memset(dst, 0, bytes);
    else {
        for (; n > 0; n --, d += dstr/3)
            d[0] = d[1] = d[2] = 0;
    }
}

static void zero_32(void *dst, size_t dstr, size_t bytes) {
    uint32_t *d = dst;
    size_t n = bytes/sizeof(float);

    if (dstr == sizeof(uint32_t))
        memset(dst, 0, bytes);
    else {
        for (; n > 0; n --, d += dstr/sizeof(uint32_t))
            *d = 0;
    }
}

sa_zero_func_t sa_get_zero_func(sa_pcm_format_t f) {

    static const sa_zero_func_t funcs[_SA_PCM_FORMAT_MAX] = {
        [SA_PCM_FORMAT_U8] = zero_u8,
        [SA_PCM_FORMAT_ULAW] = zero_ulaw,
        [SA_PCM_FORMAT_ALAW] = zero_alaw,
        [SA_PCM_FORMAT_S16_LE] = zero_16,
        [SA_PCM_FORMAT_S16_BE] = zero_16,
        [SA_PCM_FORMAT_S24_LE] = zero_24,
        [SA_PCM_FORMAT_S24_BE] = zero_24,
        [SA_PCM_FORMAT_S32_LE] = zero_32,
        [SA_PCM_FORMAT_S32_BE] = zero_32,
        [SA_PCM_FORMAT_FLOAT32_LE] = zero_32,
        [SA_PCM_FORMAT_FLOAT32_BE] = zero_32
    };

    sa_assert(f < _SA_PCM_FORMAT_MAX);

    return funcs[f];
}
