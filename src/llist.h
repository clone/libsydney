#ifndef foosydneyllistfoo
#define foosydneyllistfoo

/***
  This file is part of libsydney.

  Copyright 2008 Lennart Poettering

  libsydney is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  libsydney is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with libsydney. If not, see
  <http://www.gnu.org/licenses/>.
***/

#include "macro.h"

#define SA_LLIST_HEAD(t,head)                                           \
    t* head

#define SA_LLIST_ITEM(t,name)                                           \
    t* name##_prev, *name##_next

#define SA_LLIST_HEAD_INIT(t,head)                                      \
    (head) = (t*) NULL

#define SA_LLIST_ITEM_INIT(t,name,item)                                 \
    do {                                                                \
        t *_item = (item);                                              \
        sa_assert(_item);                                               \
        _item->name##_prev = _item->name##_next = NULL;                 \
    } while(0)

#define SA_LLIST_PREPEND(t,name,head,item)                              \
    do {                                                                \
        t **_head = &(head), *_item = (item);                           \
        sa_assert(_item);                                               \
        if ((_item->name##_next = *_head))                              \
            _item->name##_next->name##_prev = _item;                    \
        _item->name##_prev = NULL;                                      \
        *_head = _item;                                                 \
    } while (0)

#define SA_LLIST_INSERT_BEFORE(t,name,head,at,item)                     \
    do {                                                                \
        t **_head = &(head), *_item = (item), *_at = (at);              \
        sa_assert(_item);                                               \
        sa_assert(_at);                                                 \
        if ((_item->name##_prev = _at->name##_prev)) {                  \
            sa_assert(_item->name##_prev->name##_next == _at);          \
            _item->name##_prev->name##_next = _item;                    \
        } else {                                                        \
            sa_assert(*_head == _at);                                   \
            *_head = _item;                                             \
        }                                                               \
        _item->name##_next = _at;                                       \
        _at->name##_prev = _item;                                       \
    } while (0)

#define SA_LLIST_INSERT_AFTER(t,name,head,at,item)                      \
    do {                                                                \
        t *_item = (item), *_at = (at);                                 \
        sa_assert(_item);                                               \
        sa_assert(_at);                                                 \
        if ((_item->name##_next = _at->name##_next)) {                  \
            sa_assert(_item->name##_next->name##_prev == _at);          \
            _item->name##_next->name##_prev = _item;                    \
        }                                                               \
        _item->name##_prev = _at;                                       \
        _at->name##_next = _item;                                       \
    } while (0)

#define SA_LLIST_REMOVE(t,name,head,item)                               \
    do {                                                                \
        t **_head = &(head), *_item = (item);                           \
        sa_assert(_item);                                               \
        if (_item->name##_next)                                         \
            _item->name##_next->name##_prev = _item->name##_prev;       \
        if (_item->name##_prev)                                         \
            _item->name##_prev->name##_next = _item->name##_next;       \
        else {                                                          \
            sa_assert(*_head == _item);                                 \
            *_head = _item->name##_next;                                \
        }                                                               \
        _item->name##_next = _item->name##_prev = NULL;                 \
    } while(0)

#endif
