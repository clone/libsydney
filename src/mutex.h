#ifndef foosydneymutexhfoo
#define foosydneymutexhfoo

/***
  This file is part of libsydney.

  Copyright 2007-2008 Lennart Poettering

  libsydney is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  libsydney is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with libsydney. If not, see
  <http://www.gnu.org/licenses/>.
***/

#include "macro.h"

typedef struct sa_mutex sa_mutex;

sa_mutex* sa_mutex_new(sa_bool_t recursive, sa_bool_t inherit_priority);
void sa_mutex_free(sa_mutex *m);
void sa_mutex_lock(sa_mutex *m);
void sa_mutex_unlock(sa_mutex *m);

typedef struct sa_cond sa_cond;

sa_cond *sa_cond_new(void);
void sa_cond_free(sa_cond *c);
void sa_cond_signal(sa_cond *c, int broadcast);
int sa_cond_wait(sa_cond *c, sa_mutex *m);

#endif
