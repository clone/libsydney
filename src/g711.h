#ifndef foosydneyg711hfoo
#define foosydneyg711hfoo

/* g711.h - include for G711 u-law and a-law conversion routines
**
** Copyright (C) 2001 Chris Bagwell
**
** Permission to use, copy, modify, and distribute this software and its
** documentation for any purpose and without fee is hereby granted, provided
** that the above copyright notice appear in all copies and that both that
** copyright notice and this permission notice appear in supporting
** documentation.  This software is provided "as is" without express or
** implied warranty.
*/

/** Copied from sox */

#include <inttypes.h>

#ifdef FAST_ALAW_CONVERSION
extern uint8_t _sa_13linear2alaw[0x2000];
extern int16_t _sa_alaw2linear16[256];
#define sa_13linear2alaw(sw) (_sa_13linear2alaw[(sw + 0x1000)])
#define sa_alaw2linear16(uc) (_sa_alaw2linear16[uc])
#else
unsigned char sa_13linear2alaw(int16_t pcm_val);
int16_t sa_alaw2linear16(unsigned char);
#endif

#ifdef FAST_ULAW_CONVERSION
extern uint8_t _sa_14linear2ulaw[0x4000];
extern int16_t _sa_ulaw2linear16[256];
#define sa_14linear2ulaw(sw) (_sa_14linear2ulaw[(sw + 0x2000)])
#define sa_ulaw2linear16(uc) (_sa_ulaw2linear16[uc])
#else
unsigned char sa_14linear2ulaw(int16_t pcm_val);
int16_t sa_ulaw2linear16(unsigned char);
#endif

#endif
