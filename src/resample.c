/***
  This file is part of libsydney.

  Copyright 2007-2008 Lennart Poettering

  libsydney is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  libsydney is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with libsydney. If not, see
  <http://www.gnu.org/licenses/>.
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "macro.h"
#include "resample.h"

static void resample_s16(SpeexResamplerState *speex, unsigned channel, void *dst, size_t dstr, const void *src, size_t sstr, size_t in_bytes, size_t *out_bytes) {

    spx_uint32_t in_samples = (uint32_t) (in_bytes/sizeof(int16_t)), out_samples = (uint32_t) (*out_bytes/sizeof(int16_t));

    speex_resampler_set_input_stride(speex, (uint32_t) (sstr/sizeof(int16_t)));
    speex_resampler_set_input_stride(speex, (uint32_t) (dstr/sizeof(int16_t)));

    speex_resampler_process_int(speex, channel, src, &in_samples, dst, &out_samples);

    sa_assert(in_samples == in_bytes/sizeof(int16_t));

    *out_bytes = out_samples * sizeof(int16_t);
}

static void resample_f32(SpeexResamplerState *speex, unsigned channel, void *dst, size_t dstr, const void *src, size_t sstr, size_t in_bytes, size_t *out_bytes) {

    spx_uint32_t in_samples = (uint32_t) (in_bytes/sizeof(float)), out_samples = (uint32_t) (*out_bytes/sizeof(float));

    speex_resampler_set_input_stride(speex, (uint32_t) (sstr/sizeof(float)));
    speex_resampler_set_input_stride(speex, (uint32_t) (dstr/sizeof(float)));

    speex_resampler_process_float(speex, channel, src, &in_samples, dst, &out_samples);

    sa_assert(in_samples == in_bytes/sizeof(float));

    *out_bytes = out_samples * sizeof(float);
}

sa_resample_func_t sa_get_resample_func(sa_pcm_format_t f) {

    static const sa_resample_func_t funcs[_SA_PCM_FORMAT_MAX] = {
        [SA_PCM_FORMAT_S16_NE] = resample_s16,
        [SA_PCM_FORMAT_FLOAT32_NE] = resample_f32
    };

    sa_assert(f < _SA_PCM_FORMAT_MAX);

    return funcs[f];
}
