/***
  This file is part of libsydney.

  Copyright 2007-2008 Lennart Poettering

  libsydney is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  libsydney is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with libsydney. If not, see
  <http://www.gnu.org/licenses/>.
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <inttypes.h>

#include "macro.h"
#include "interleave.h"

static void interleave8(void *_dst, size_t dstr, const void *_src, size_t sstr, size_t bytes) {
    uint8_t *dst = _dst;
    const uint8_t *src = _src;

    for (; bytes > 0; bytes--, src += sstr, dst += dstr)
        *dst = *src;
}

static void interleave16(void *_dst, size_t dstr, const void *_src, size_t sstr, size_t bytes) {
    uint16_t *dst = _dst;
    const uint16_t *src = _src;
    size_t n = bytes / sizeof(uint16_t);

    for (; n > 0; n--, src += sstr/sizeof(uint16_t), dst += dstr/sizeof(uint16_t))
        *dst = *src;
}

static void interleave24(void *_dst, size_t dstr, const void *_src, size_t sstr, size_t bytes) {
    uint8_t *dst = _dst;
    const uint8_t *src = _src;
    size_t n = bytes / 3;

    for (; n > 0; n--, src += sstr/3, dst += dstr/3) {
        dst[0] = src[0];
        dst[1] = src[1];
        dst[2] = src[2];
    }
}

static void interleave32(void *_dst, size_t dstr, const void *_src, size_t sstr, size_t bytes) {
    uint32_t *dst = _dst;
    const uint32_t *src = _src;
    size_t n = bytes / sizeof(uint32_t);

    for (; n > 0; n--, src += sstr/sizeof(uint32_t), dst += dstr/sizeof(uint32_t))
        *dst = *src;
}

sa_interleave_func_t sa_get_interleave_func(sa_pcm_format_t f) {

    static const sa_interleave_func_t funcs[_SA_PCM_FORMAT_MAX] = {
        [SA_PCM_FORMAT_U8] = interleave8,
        [SA_PCM_FORMAT_ULAW] = interleave8,
        [SA_PCM_FORMAT_ALAW] = interleave8,
        [SA_PCM_FORMAT_S16_LE] = interleave16,
        [SA_PCM_FORMAT_S16_BE] = interleave16,
        [SA_PCM_FORMAT_S24_LE] = interleave24,
        [SA_PCM_FORMAT_S24_BE] = interleave24,
        [SA_PCM_FORMAT_S32_LE] = interleave32,
        [SA_PCM_FORMAT_S32_BE] = interleave32,
        [SA_PCM_FORMAT_FLOAT32_LE] = interleave32,
        [SA_PCM_FORMAT_FLOAT32_BE] = interleave32,
    };

    sa_assert(f < _SA_PCM_FORMAT_MAX);

    return funcs[f];
}
