/***
  This file is part of libsydney.

  Copyright 2007-2008 Lennart Poettering

  libsydney is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  libsydney is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with libsydney. If not, see
  <http://www.gnu.org/licenses/>.
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <errno.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>

#include "sydney.h"
#include "macro.h"

#define FREQ 440

static const float data[4] = { 0.0, 1.0, 0.0, -1.0 };

static int callback(sa_stream *s, sa_event_t e) {
    switch (e) {
        case SA_EVENT_INIT_THREAD:
            printf("Thread initialized.\n");
            return 0;

        case SA_EVENT_ERROR: {
            int err;
            sa_assert_se(sa_stream_get_event_error(s, &err) == 0);
            printf("Error: %s\n", sa_strerror(err));
            return -1;
        }

        case SA_EVENT_NOTIFY:
            printf("Notified.\n");
            return 0;

        case SA_EVENT_REQUEST_IO:

            sa_assert_se(sa_stream_write(s, data, sizeof(data)) == 0);
            return 0;

        case SA_EVENT_XRUN:
        case _SA_EVENT_MAX:
            ;
    }

    sa_assert_not_reached();
}

int main(int argc, char *argv[]) {

    sa_stream *s;

    sa_assert_se(sa_stream_create_pcm(&s, SA_MODE_WRONLY, SA_PCM_FORMAT_FLOAT32_NE, FREQ * 4, 1) == 0);
    sa_assert_se(sa_stream_change_device(s, "/dev/dsp1") == 0);
    sa_assert_se(sa_stream_open(s) == 0);

/*     sa_assert_se(sa_stream_start_thread(s, callback)); */

    sleep(20);

/*     sa_assert_se(sa_stream_stop_thread(s)); */

    sa_assert_se(sa_stream_drain(s) == 0);

    sa_assert_se(sa_stream_destroy(s) == 0);

    return 0;
}
