#ifndef foosydneybbufferhfoo
#define foosydneybbufferhfoo

/***
  This file is part of libsydney.

  Copyright 2007-2008 Lennart Poettering

  libsydney is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  libsydney is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with libsydney. If not, see
  <http://www.gnu.org/licenses/>.
***/

#include <sys/types.h>

#include "macro.h"

/* Simple bounce buffer management routines */

typedef struct sa_bbuffer {
    void **data;
    size_t *size;
    unsigned nchannels;
    size_t sample_size;
} sa_bbuffer_t;

int sa_bbuffer_init(sa_bbuffer_t *b, unsigned nchannels, size_t sample_size);
void sa_bbuffer_done(sa_bbuffer_t *b);
void* sa_bbuffer_get(sa_bbuffer_t *b, unsigned channel, size_t size, sa_bool_t interleave);

#endif
