#ifndef foosydneyproplisthfoo
#define foosydneyproplisthfoo

/***
  This file is part of libsydney.

  Copyright 2007-2008 Lennart Poettering

  libsydney is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  libsydney is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with libsydney. If not, see
  <http://www.gnu.org/licenses/>.
***/

#include <stdarg.h>

#include "sydney.h"
#include "mutex.h"

#define N_HASHTABLE 31

typedef struct sa_prop {
    char *key;
    size_t nbytes;
    struct sa_prop *next_in_slot, *next_item, *prev_item;
} sa_prop;

#define SA_PROP_DATA(p) ((void*) ((char*) (p) + SA_ALIGN(sizeof(sa_prop))))

struct sa_proplist {
    sa_mutex *mutex;

    sa_prop *prop_hashtable[N_HASHTABLE];
    sa_prop *first_item;
};

int sa_proplist_merge(sa_proplist **_a, sa_proplist *b, sa_proplist *c);
sa_bool_t sa_proplist_contains(sa_proplist *p, const char *key);

/* Both of the following two functions are not locked! Need manual locking! */
sa_prop* sa_proplist_get_unlocked(sa_proplist *p, const char *key);
const char* sa_proplist_gets_unlocked(sa_proplist *p, const char *key);

int sa_proplist_merge_ap(sa_proplist *p, va_list ap);
int sa_proplist_from_ap(sa_proplist **_p, va_list ap);

#endif
