#ifndef foosydneyvolscalehfoo
#define foosydneyvolscalehfoo

/***
  This file is part of libsydney.

  Copyright 2007-2008 Lennart Poettering

  libsydney is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  libsydney is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with libsydney. If not, see
  <http://www.gnu.org/licenses/>.
***/

#include <sys/types.h>
#include <inttypes.h>

#include "sydney.h"

typedef void (*sa_volscale_func_t) (void *dst, size_t dstr, const void *src, size_t sstr, int32_t factor, int32_t divisor, size_t bytes);

sa_volscale_func_t sa_get_volscale_func(sa_pcm_format_t f);

#endif
