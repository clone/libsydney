#ifndef foosydneybufferqhfoo
#define foosydneybufferqhfoo

/***
  This file is part of libsydney.

  Copyright 2007-2008 Lennart Poettering

  libsydney is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  libsydney is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with libsydney. If not, see
  <http://www.gnu.org/licenses/>.
***/

#include <inttypes.h>

#include "macro.h"
#include "llist.h"
#include "sydney.h"

typedef enum sa_bufferq_item_type {
    SA_BUFFERQ_ITEM_CONCATENATED,
    SA_BUFFERQ_ITEM_DYNAMIC,
    SA_BUFFERQ_ITEM_STATIC
} sa_bufferq_item_type_t;

typedef struct sa_bufferq_item {
    void *data;
    int64_t idx;
    size_t size;
    sa_bufferq_item_type_t type;
    SA_LLIST_ITEM(struct sa_bufferq_item, bufferq);
} sa_bufferq_item_t;

typedef struct sa_bufferq {
    SA_LLIST_HEAD(sa_bufferq_item_t, *items);
    sa_bufferq_item_t **last;
    int64_t read_index, write_index, end_index;
    unsigned nchannels;
} sa_bufferq_t;

int sa_bufferq_init(sa_bufferq_t *q, unsigned nchannels);

void sa_bufferq_done(sa_bufferq_t *q);

int sa_bufferq_push(sa_bufferq_t *q, unsigned channel, const void *data, size_t nbytes, int64_t offset, sa_seek_t whence, sa_bufferq_item_type_t type);
int sa_bufferq_realloc(sa_bufferq_t *q);

void sa_bufferq_get(sa_bufferq_t *q, void *i[], size_t *bytes);
void sa_bufferq_drop(sa_bufferq_t *q, int64_t bytes);

#endif
