#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "llist.h"

struct foobar {
    SA_LLIST_ITEM(struct foobar, list);
};


static SA_LLIST_HEAD(struct foobar, list);

int main(int argc, char *argv[]) {
    struct foobar a, b, c;

    SA_LLIST_HEAD_INIT(struct foobar, list);

    SA_LLIST_ITEM_INIT(struct foobar, list, &a);
    SA_LLIST_ITEM_INIT(struct foobar, list, &b);
    SA_LLIST_ITEM_INIT(struct foobar, list, &c);

    SA_LLIST_PREPEND(struct foobar, list, list, &a);
    SA_LLIST_INSERT_BEFORE(struct foobar, list, list, &a, &b);
    SA_LLIST_INSERT_AFTER(struct foobar, list, list, &a, &c);

    sa_assert(list == &b);
    sa_assert(list->list_next == &a);
    sa_assert(list->list_next->list_next == &c);

    SA_LLIST_REMOVE(struct foobar, list, list, &a);
    SA_LLIST_REMOVE(struct foobar, list, list, &b);
    SA_LLIST_REMOVE(struct foobar, list, list, &c);

    sa_assert(!list);

    return 0;
}
