#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>

#include "asyncq.h"
#include "thread.h"
#include "macro.h"

#define ITERATIONS_MAX 1000

static void thread(void *userdata) {
    sa_asyncq_t *q = userdata;
    int i;

    for (i = 0; i < ITERATIONS_MAX; i++) {
        sa_asyncq_item_t *i;

        i = sa_asyncq_get(q);
        sa_assert(i);

        sa_asyncq_push(q, i);

        if (rand() & 1)
            sa_thread_yield();
    }
}

int main(int argc, char *argv[]) {
    int j;
    sa_thread_t *t;
    sa_asyncq_t q;
    sa_asyncq_item_t *i;

    sa_assert_success(sa_asyncq_init(&q));

    t = sa_thread_new(thread, &q);
    sa_assert(t);

    for (j = 0; j < ITERATIONS_MAX; j++) {

        do {
            i = sa_asyncq_pop(&q, 0);
            printf("%s ", i ? "gotcha" : "miss");

            if (i)
                sa_asyncq_recycle(&q, i);
        } while (i);

        if (rand() & 1)
            sa_thread_yield();
    }

    printf("\n");

    sa_thread_free(t);

    while (sa_asyncq_pop(&q, 1))
        ;

    sa_asyncq_done(&q);


    return 0;
}
